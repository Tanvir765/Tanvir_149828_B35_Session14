<html>
<head>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<form action="" method="post" align="center">
    <h1>About Book Title</h1>
    <div class="form-group">
        <label for="AName">Author Name:</label><br>
        <input type="text" name="authorname" placeholder="Enter Author Name">
        </div>
    <div class="form-group">
        <br>
        Book Name:<br>
        <input type="text" name="bookname" placeholder="Enter Book Name">
    </div>
        <br>
    <div>
        Catalogue:
        <input type="text" name="catalogue" placeholder="Enter Catalogue">
    </div>
        <br>
    <div>
        Price:<br>
        <input type="text" name="price" placeholder="Enter price">
        <br>
    </div>
    <br>
    <button type="submit" class="btn btn-primary">Submit</button>
    <button type="delete" class="btn btn-danger">Delete</button>
    <button type="update" class="btn btn-info">Update</button>
    <button type="add&save" class="btn btn-success">Add & Save</button>
    <button type="backtolist" class="btn btn-default">Back To List</button>
</form>
</body>
</html>